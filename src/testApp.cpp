#include "testApp.h"

/* Note on OS X, you must have this in the Run Script Build Phase of your project. 
where the first path ../../../addons/ofxLeapMotion/ is the path to the ofxLeapMotion addon. 

cp -f ../../../addons/ofxLeapMotion/libs/lib/osx/libLeap.dylib "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/MacOS/libLeap.dylib"; install_name_tool -change ./libLeap.dylib @executable_path/libLeap.dylib "$TARGET_BUILD_DIR/$PRODUCT_NAME.app/Contents/MacOS/$PRODUCT_NAME";

   If you don't have this you'll see an error in the console: dyld: Library not loaded: @loader_path/libLeap.dylib
*/

/* 
 createGeoSphere(...) is taken from https://sites.google.com/site/ofauckland/examples/geographic-grid-tessellated-sphere
 */
ofMesh createGeoSphere(int stacks=32, int slices=32) {
    ofMesh mesh;
    
    //add vertices
    mesh.addVertex(ofVec3f(0,0,1));
    
    for (int i=1; i<stacks; i++) {
        double phi = PI * double(i)/stacks;
        double cosPhi = cos(phi);
        double sinPhi = sin(phi);
        for (int j=0; j<slices; j++) {
            double theta = TWO_PI * double(j)/slices;
            mesh.addVertex(ofVec3f(cos(theta)*sinPhi, sin(theta)*sinPhi, cosPhi));
        }
    }
    mesh.addVertex(ofVec3f(0,0,-1));
    
    //top row triangle fan
    for (int j=1; j<slices; j++) {
        mesh.addTriangle(0,j,j+1);
    }
    mesh.addTriangle(0,slices,1);
    
    //triangle strips
    for (int i=0; i < stacks-2; i++) {
        int top = i*slices + 1;
        int bottom = (i+1)*slices + 1;
        
        for (int j=0; j<slices-1; j++) {
            mesh.addTriangle(bottom+j, bottom+j+1, top+j+1);
            mesh.addTriangle(bottom+j, top+j+1, top+j);
        }
        
        mesh.addTriangle(bottom+slices-1, bottom, top);
        mesh.addTriangle(bottom+slices-1, top, top+slices-1);
    }
    
    //bottom row triangle fan
    int last = mesh.getNumVertices()-1;
    for (int j=last-1; j>last-slices; j--) {
        mesh.addTriangle(last,j,j-1);
    }
    mesh.addTriangle(last,last-slices,last-1);
    
    return mesh;
}

//--------------------------------------------------------------
void testApp::setup(){

    ofSetFrameRate(60);
    ofSetVerticalSync(true);
	ofSetLogLevel(OF_LOG_VERBOSE);

	leap.open(); 

	l1.setPosition(200, 300, 50);
	l2.setPosition(-200, -200, 50);

	cam.setOrientation(ofPoint(-20, 0, 0));

	glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    
    
    for (int i = 0; i < 5; i++)
        color.push_back(ofColor(125,125,125));
    
    circleSize = 0;
    accuracy = 0;
    lastFinger = 0;
    finger = 0;
    
    rotation = ofQuaternion(1,1,1,1);
    
    meshSphere = createGeoSphere(24,24);
}



bool handInArea(ofPoint pos){
    bool isit = true;
    isit = pos.x > 100 ? false : isit;
    isit = pos.y > 100 ? false : isit;
    isit = pos.z > 200 ? false : isit;
    isit = pos.x < -100 ? false : isit;
    isit = pos.y < -100 ? false : isit;
    isit = pos.z < 0 ? false : isit;
    return isit;
}

ofColor colorOut(float x,float y,float z){
    ofColor color;
    float xP = max(-100.0f, min(100.0f, x));
    float yP = max(-100.0f, min(100.0f, y));
    float zP = max(0.0f, min(200.0f, z));
    color.r = (int)ofMap(xP, -100.0f, 100.0f, 0.0f, 255.0f);
    color.g = (int)ofMap(yP, -100.0f, 100.0f, 0.0f, 255.0f);
    color.b = (int)ofMap(zP, 0.0f, 200.0f, 0.0f, 255.0f);
    return color;
}


//--------------------------------------------------------------
void testApp::update(){

	fingersFound.clear();
	
	//here is a simple example of getting the hands and using them to draw trails from the fingertips. 
	//the leap data is delivered in a threaded callback - so it can be easier to work with this copied hand data
	
	//if instead you want to get the data as it comes in then you can inherit ofxLeapMotion and implement the onFrame method. 
	//there you can work with the frame data directly.

    //Option 1: Use the simple ofxLeapMotionSimpleHand - this gives you quick access to fingers and palms. 
    
    simpleHands = leap.getSimpleHands();
    
    if( leap.isFrameNew() && simpleHands.size() ){
    
        leap.setMappingX(-230, 230, -ofGetWidth()/2, ofGetWidth()/2);
		leap.setMappingY(90, 490, -ofGetHeight()/2, ofGetHeight()/2);
        leap.setMappingZ(-150, 150, -200, 200);
    
        for(int i = 0; i < simpleHands.size(); i++){
        
            for(int j = 0; j < simpleHands[i].fingers.size(); j++){
                int id = simpleHands[i].fingers[j].id;
            
                ofPolyline & polyline = fingerTrails[id]; 
                ofPoint pt = simpleHands[i].fingers[j].pos;
                
                //if the distance between the last point and the current point is too big - lets clear the line 
                //this stops us connecting to an old drawing
                if( polyline.size() && (pt-polyline[polyline.size()-1] ).length() > 50 ){
                    polyline.clear(); 
                }
                
                //add our point to our trail
                polyline.addVertex(pt); 
                
                //store fingers seen this frame for drawing
                fingersFound.push_back(id);
            }
        }
    }
    
    for(ofxLeapMotionSimpleHand simpleHand : simpleHands){
        
        if(lastFinger == simpleHand.fingers.size())
            accuracy++;
        else
            accuracy = 0;
        // accuracy += ( lastFinger == simpleHand.fingers.size() );
        
        lastFinger = simpleHand.fingers.size();
        
        
        if (accuracy > 50){
            finger = lastFinger;
        
            if(finger == 1) {
                
                color[0] = colorOut(simpleHand.handPos.x, simpleHand.handPos.y, simpleHand.handPos.z);
                
            } else if(finger == 2 && simpleHand.fingers.size() ==2) {
                
                color[1] = colorOut(simpleHand.handPos.x, simpleHand.handPos.y, simpleHand.handPos.z);
                circleSize = 0.1f * (float)simpleHand.fingers[0].pos.distance(simpleHand.fingers[1].pos) + 0.9f * (float)circleSize;
                
                
            } else if(finger == 3) {
                color[2] = colorOut(simpleHand.handPos.x, simpleHand.handPos.y, simpleHand.handPos.z);
                
            } else if(finger == 4) {
                
                color[3] = colorOut(simpleHand.handPos.x, simpleHand.handPos.y, simpleHand.handPos.z);
                
            } else if(finger == 5) {
                
                color[4] = colorOut(simpleHand.handPos.x, simpleHand.handPos.y, simpleHand.handPos.z);
                
            }
        }
    }

	//IMPORTANT! - tell ofxLeapMotion that the frame is no longer new.
	leap.markFrameAsOld();	
}

//--------------------------------------------------------------
void testApp::draw(){
	ofDisableLighting();
    
    ofBackgroundGradient(color[3], color[4],  OF_GRADIENT_LINEAR);
    
    ofDrawBitmapString("position: 1 finger\nsize & sphere color: 2 fingers\nwireframe color: 3 fingers\nbackground up color: 4 fingers\nbackground down color: 5 fingers\nnothing: no fingers", 20, 20);
    
    for(ofxLeapMotionSimpleHand simpleHand : simpleHands){
        if (handInArea(simpleHand.handPos)){
            ofDrawBitmapString("alright", 20, 160);
        }
            ofDrawBitmapString("accuracy: " + ofToString(accuracy) + " finger " + ofToString(lastFinger) + " r " + ofToString((int)color[lastFinger-1].r) + " g " + ofToString((int)color[lastFinger-1].g) + " b " + ofToString((int)color[lastFinger-1].b), 20, 180);
    }
    ofPushStyle();
    ofPushMatrix();
    ofTranslate(ofMap((int)color[0].r, 0, 255, 0, ofGetWidth()), ofMap((int)color[0].g, 0, 255, ofGetHeight(), 0), ofMap((int)color[0].b, 0, 255, -ofGetHeight(), ofGetHeight()));
    
    rotation.set(
                 ofMap((float)color[2].r, 0.0f, 255.0f, 0.0f, 1.0f),
                 ofMap((float)color[2].g, 0.0f, 255.0f, 0.0f, 1.0f),
                 ofMap((float)color[2].b, 0.0f, 255.0f, 0.0f, 1.0f),
                 ofMap( abs( ((float)color[2].r+(float)color[2].g+(float)color[2].b)-375.0f ) , 0.0f, 375.0f, 0.0f, 100.0f) + ofGetFrameNum()%360
                 );
    cout << rotation.asVec4().w << " " << rotation.asVec4().x << " " << rotation.asVec4().y << " " << rotation.asVec4().z << endl;
    ofRotate(rotation.asVec4().w, rotation.asVec4().x, rotation.asVec4().y, rotation.asVec4().z);
    meshSphere.draw();
    ofSetColor(color[1]);
    ofSpherePrimitive(circleSize-10,8).draw();
    ofSetColor(color[2]);
    ofSpherePrimitive(circleSize,8).drawWireframe();
    
    ofPopMatrix();
    ofPopStyle();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
  
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){
    
}

//--------------------------------------------------------------
void testApp::exit(){
    // let's close down Leap and kill the controller
    leap.close();
}
