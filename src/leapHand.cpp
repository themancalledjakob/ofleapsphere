//
//  leapHand.cpp
//  ofxLeapMotion
//
//  Created by Jakob Schlötter on 10/12/13.
//
//

#include "leapHand.h"

void leapHand::update(){
    // making of the mesh -> later, later, later
    
}

void leapHand::draw(){
    ofPushMatrix();
    ofPushStyle();
    
    ofSetColor(255, 0, 0);
    
    //rotate the hand by the downwards normal
    ofQuaternion q;
    q.makeRotate(ofPoint(0, -1, 0), palm.normal);
    ofMatrix4x4 m;
    q.get(m);
    glMultMatrixf(m.getPtr());
    
    ofDrawBox(ofPoint(palm.position), 50);
    
    ofPopStyle();
    ofPopMatrix();
}